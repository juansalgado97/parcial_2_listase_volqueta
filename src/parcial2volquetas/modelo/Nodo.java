/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial2volquetas.modelo;

/**
 *
 * @author SALGADO
 */
public class Nodo {

    private Volqueta dato;
    private Nodo siguiente;

    public Nodo(Volqueta dato) {
        this.dato = dato;
    }

    public Volqueta getDato() {
        return dato;
    }

    public void setDato(Volqueta dato) {
        this.dato = dato;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    
    
}
