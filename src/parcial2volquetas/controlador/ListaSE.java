/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial2volquetas.controlador;


import parcial2volquetas.modelo.Nodo;
import parcial2volquetas.modelo.Volqueta;

/**
 *
 * @author SALGADO
 */
public class ListaSE {

    private Nodo cabeza;

    public ListaSE() {
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    public void adicionarNodoAlInicio(Volqueta dato) {
        if (cabeza != null) {
            Nodo nuevo = new Nodo(dato);
            cabeza = nuevo;
        } else {
            cabeza = new Nodo(dato);
        }
    }

    public void adicionarNodo(Volqueta dato) {
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            temp.setSiguiente(new Nodo(dato));
        } else {
            cabeza = new Nodo(dato);
        }
    }

    public int contarNodos() {
        if (cabeza != null) {
            int cont = 0;
            Nodo temp = cabeza;
            while (temp != null) {
                cont++;
                temp.getSiguiente();
            }
            return cont;
        } else {//No hay Volquetas
            return 0;
        }

    }


    public Nodo irAlUltimo() {
        Nodo temp = cabeza;

        while (temp != null) {
            if (temp.getSiguiente() == null) {
                return temp;
            }
        }
        return null;
    }

    
    public boolean listarMenoresDeDosToneladas() {
        if (cabeza != null) {
            Nodo temp = cabeza;
            ListaSE listaCopia = new ListaSE();

            while (temp != null) {
                if (temp.getDato().getCapacidad() < 2) {
                    listaCopia.adicionarNodo(temp.getDato());
                    temp = temp.getSiguiente();
                } 
                else {
                    temp = temp.getSiguiente();
                }
            }
            cabeza = listaCopia.getCabeza();
            return true;
        }
        return false;
    }
    
    public Nodo obtenerVolquetaConMasCapacidad(){
        if (cabeza != null) {
            Nodo temp = cabeza;
            float mayorCapacidad = cabeza.getDato().getCapacidad();
            Nodo volquetaMayor = cabeza;
            
            while (temp != null) {  
                
                if (temp.getDato().getCapacidad() > mayorCapacidad) {
                    mayorCapacidad = temp.getDato().getCapacidad();
                    volquetaMayor = temp;
                    temp = temp.getSiguiente();
                }
                temp = temp.getSiguiente();
            }
            return volquetaMayor;
        }
        return  null;
    }
    
    public Nodo encontrarElUltimo(){
        if (cabeza != null) {
            Nodo temp = cabeza;
            
            while (temp != null) {                
                if (temp.getSiguiente() == null) {
                    return temp;
                }
                temp = temp.getSiguiente();
            }
        }
        return null;
    }
}
