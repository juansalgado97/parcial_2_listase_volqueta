/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial2volquetas.modelo;

/**
 *
 * @author SALGADO
 */
public class Volqueta {
    private String marca;
    private float capacidad;
    private String tipoCarga;

    public Volqueta() {
    }

    public Volqueta(String marca, float capacidad, String tipoCarga) {
        this.marca = marca;
        this.capacidad = capacidad;
        this.tipoCarga = tipoCarga;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public String getTipoCarga() {
        return tipoCarga;
    }

    public void setTipoCarga(String tipoCarga) {
        this.tipoCarga = tipoCarga;
    }

    @Override
    public String toString() {
        return "Volqueta{" + "marca=" + marca + ", capacidad=" + capacidad + ", tipoCarga=" + tipoCarga + '}';
    }
    
    
    
}
